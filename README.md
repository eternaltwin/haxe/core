# Eternaltwin Haxe core

Extensions to the Haxe standard library for Eternal-Twin.

## Defines

The code in this repository depends on the following Haxe defines.

<!--
## `analysis`

Use this define when running code analysis.

When this define is enabled, this library will try its best to avoid causing
`not supported on this platform` error, regardless of the target platform.

The main use-case is to generate RunTime Type Information (RTTI) metadata from neko for
other compilation targets. For example, you can use `-D analysis -D flash8`.
-->

## `flash8`

Generate code for the AVM1 platform.

Standard define, enabled automatically when targeting `flash8`.

## `js`

Generate code for the browser or Node.

Standard define, enabled automatically when targeting Node or the browser.
