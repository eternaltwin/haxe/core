package etwin.flash;

typedef KeyListener = {
function onKeyDown(): Void;

function onKeyUp(): Void;
}
