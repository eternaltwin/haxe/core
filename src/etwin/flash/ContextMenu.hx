package etwin.flash;

import etwin.flash.ContextMenuItem;

@:native("ContextMenu")
extern class ContextMenu {
  /**
    An object that has the following Boolean properties: `zoom`, `quality`, `play`, `loop`, `rewind`, `forward_back`, and `print`.
  **/
  public var builtInItems: Dynamic;

  /**
    An array of ContextMenuItem objects.
  **/
  public var customItems: Array<ContextMenuItem>;

  /**
    Creates a new ContextMenu object.
  **/
  public function new(?callbackFunction: Dynamic -> ContextMenu -> Void): Void;

  /**
    Hides all built-in menu items (except Settings) in the specified ContextMenu object.
  **/
  function hideBuiltInItems(): Void;

  /**
    Creates a copy of the specified ContextMenu object.
  **/
  function copy(): ContextMenu;

  /**
    Called when a user invokes the Flash Player context menu, but before the menu is actually displayed.
  **/
  dynamic function onSelect(item: Dynamic, itemMenu: ContextMenu): Void;
}
