package etwin.flash;

typedef MouseListener = {
function onMouseDown(): Void;

function onMouseMove(): Void;

function onMouseUp(): Void;

function onMouseWheel(delta: Float, scrollTarget: String): Void;
}
