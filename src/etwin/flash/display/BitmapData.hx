package etwin.flash.display;

import etwin.flash.geom.Matrix;
import etwin.flash.filters.BitmapFilter;
import etwin.flash.geom.Point;
import etwin.flash.geom.Rectangle;

@:native("flash.display.BitmapData")
extern class BitmapData {
  /**
    Returns a new BitmapData object that contains a bitmap image representation of the symbol that is identified by a specified linkage ID in the library.
  **/
  static function loadBitmap(id: String): BitmapData;

  /**
    The width of the bitmap image in pixels.
  **/
  var width(default, null): Int;

  /**
    The height of the bitmap image in pixels.
  **/
  var height(default, null): Int;

  /**
    The rectangle that defines the size and location of the bitmap image.
  **/
  var rectangle(default, null): Rectangle<Int>;

  /**
    Defines whether the bitmap image supports per-pixel transparency.
  **/
  var transparent(default, null): Bool;

  /**
    Creates a BitmapData object with a specified width and height.
  **/
  function new(width: Int, height: Int, ?transparent: Bool, ?fillcolor: Int): Void;

  /**
    Returns an integer that represents an RGB pixel value from a BitmapData object at a specific point (x, y).
  **/
  function getPixel(x: Int, y: Int): Int;

  /**
    Sets the color of a single pixel of a BitmapData object.
  **/
  function setPixel(x: Int, y: Int, color: Int): Void;

  /**
    Returns an ARGB color value that contains alpha channel data and RGB data.
  **/
  function getPixel32(x: Int, y: Int): Int;

  /**
    Sets the color and alpha transparency values of a single pixel of a BitmapData object.
  **/
  function setPixel32(x: Int, y: Int, color: Int): Void;

  /**
    Fills a rectangular area of pixels with a specified ARGB color.
  **/
  function fillRect(r: Rectangle<Int>, color: Int): Void;

  /**
    Provides a fast routine to perform pixel manipulation between images with no stretching, rotation, or color effects.
  **/
  function copyPixels(src: BitmapData, srcRect: Rectangle<Int>, dst: Point<Int>, ?alphaBitmap: BitmapData, ?alphaPoint: Point<Int>, ?mergeAlpha: Bool): Void;

  /**
    Takes a source image and a filter object and generates the filtered image.
  **/
  function applyFilter(source: BitmapData, sourceRect: Rectangle<Int>, dest: Point<Int>, filter: BitmapFilter): Int;

  /**
    Scrolls an image by a certain (x, y) pixel amount.
  **/
  function scroll(dx: Int, dy: Int): Void;

  /**
    Tests pixel values in an image against a specified threshold and sets pixels that pass the test to new color values.
  **/
  function threshold(src: BitmapData, srcRect: Rectangle<Int>, dstPoint: Point<Int>, op: String, threshold: Int, ?color: Int, ?mask: Int, ?copy: Bool): Int;

  /**
    Draws a source image or movie clip onto a destination image, using the Flash Player vector renderer.
  **/
  function draw(source: Dynamic, ?matrix: Matrix, ?colortrans: ColorTransform, ?blendMode: Dynamic, ?clipRect: Rectangle<Int>, ?smooth: Bool): Void;

  /**
    Performs a pixel dissolve either from a source image to a destination image or by using the same image.
  **/
  function pixelDissolve(src: BitmapData, srcRect: Rectangle<Int>, dst: Point<Int>, ?seed: Int, ?npixels: Int, ?fillColor: Int): Int;

  /**
    Performs a flood fill operation on an image starting at an (x, y) coordinate and filling with a certain color.
  **/
  function floodFill(x: Int, y: Int, color: Int): Void;

  /**
    Determines a rectangular region that fully encloses all pixels of a specified color within the bitmap image.
  **/
  function getColorBoundsRect(mask: Int, color: Int, ?fillColor: Bool): Rectangle<Int>;

  /**
    Generates a Perlin noise image.
  **/
  function perlinNoise(x: Int, y: Int, num: Int, seed: Int, stitch: Bool, noise: Bool, ?channels: Int, ?gray: Bool, ?offsets: Array<Point<Float>>): Void;

  /**
    Adjusts the color values in a specified area of a bitmap image by using a ColorTransform object.
  **/
  function colorTransform(r: Rectangle<Int>, trans: ColorTransform): Void;

  /**
    Performs pixel-level hit detection between one bitmap image and a point, rectangle or other bitmap image.
  **/
  function hitTest(firstPoint: Point<Int>, firstAlpha: Int, object: Dynamic, ?secondPoint: Point<Int>, ?secondAlpha: Int): Bool;

  /**
    Remaps the color channel values in an image that has up to four arrays of color palette data, one for each channel.
  **/
  function paletteMap(source: BitmapData, srcRect: Rectangle<Int>, dst: Point<Int>, ?reds: Array<Dynamic>, ?greens: Array<Dynamic>, ?blues: Array<Dynamic>, ?alphas: Array<Dynamic>): Void;

  /**
    Performs per-channel blending from a source image to a destination image.
  **/
  function merge(src: BitmapData, srcRect: Rectangle<Int>, dst: Point<Int>, redMult: Int, greenMult: Int, blueMult: Int, alphaMult: Int): Void;

  /**
    Fills an image with pixels representing random noise.
  **/
  function noise(seed: Int, ?low: Int, ?high: Int, ?channels: Int, ?gray: Bool): Void;

  /**
    Transfers data from one channel of another BitmapData object or the current BitmapData object into a channel of the current BitmapData object.
  **/
  function copyChannel(source: BitmapData, sourceRect: Rectangle<Int>, dest: Point<Int>, sourceChannel: Int, destChannel: Int): Void;

  /**
    Returns a new BitmapData object that is a clone of the original instance with an exact copy of the contained bitmap.
  **/
  function clone(): BitmapData;

  /**
    Frees memory that is used to store the BitmapData object.
  **/
  function dispose(): Void;

  /**
    Determines the destination rectangle that the `applyFilter()` method call affects, given a BitmapData object, a source rectangle, and a filter object.
  **/
  function generateFilterRect(sourceRect: Rectangle<Int>, filter: BitmapFilter): Rectangle<Int>;

  /**
    Compares two BitmapData objects.
  **/
  function compare(otherBitmapData: BitmapData): Dynamic;
}
