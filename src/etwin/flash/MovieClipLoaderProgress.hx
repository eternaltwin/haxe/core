package etwin.flash;

typedef MovieClipLoaderProgress = {
var bytesLoaded: Int;
var bytesTotal: Int;
}
