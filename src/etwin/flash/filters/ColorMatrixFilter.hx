package etwin.flash.filters;

import etwin.flash.filters.BitmapFilter;

@:native("flash.filters.ColorMatrixFilter")
@:final
extern class ColorMatrixFilter extends BitmapFilter {
  var matrix: Array<Dynamic>;

  function new(?matrix: Array<Dynamic>): Void;
	override function clone(): ColorMatrixFilter;
}
