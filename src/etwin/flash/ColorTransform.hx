package etwin.flash;

typedef ColorTransform = {
var ra: Float;
var rb: Float;
var ga: Float;
var gb: Float;
var ba: Float;
var bb: Float;
var aa: Float;
var ab: Float;
}
