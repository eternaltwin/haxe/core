package etwin.flash;

typedef MovieClipBounds = {
var xMin: Float;
var xMax: Float;
var yMin: Float;
var yMax: Float;
}
