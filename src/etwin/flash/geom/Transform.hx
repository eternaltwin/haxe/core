package etwin.flash.geom;

import etwin.flash.geom.ColorTransform;
import etwin.flash.geom.Matrix;
import etwin.flash.geom.Rectangle;
import etwin.flash.MovieClip;

@:native("flash.geom.Transform")
extern class Transform {
  var matrix: Matrix;
  var concatenatedMatrix: Matrix;
  var colorTransform: ColorTransform;
  var concatenatedColorTransform: ColorTransform;
  var pixelBounds: Rectangle<Float>;

  function new(mc: MovieClip): Void;
}
