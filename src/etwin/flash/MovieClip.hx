package etwin.flash;

import etwin.flash.MovieClip;
import etwin.flash.MovieClipBounds;
import etwin.flash.display.BitmapData;
import etwin.flash.filters.BitmapFilter;
import etwin.flash.geom.Matrix;
import etwin.flash.geom.Rectangle;
import etwin.flash.geom.Transform;
import etwin.flash.MovieClipBounds;
import etwin.flash.TextField;
import etwin.flash.TextSnapshot;

@:native("MovieClip")
extern class MovieClip {
  /**
    The alpha transparency value of the movie clip.
  **/
  public var _alpha: Float;

  /**
    Returns the number of the frame in which the playhead is located in the movie clip's timeline.
  **/
  public var _currentframe(default, null): Int;

  /**
    Returns the absolute path in slash-syntax notation of the movie clip instance on which this movie clip was dropped.
  **/
  public var _droptarget(default, null): String;

  /**
    Specifies whether you can programmatically assign focus to a movie clip using Selection.setFocus().
  **/
  public var _focusrect: Bool;

  /**
    The number of frames that are loaded from a streaming SWF file.
  **/
  public var _framesloaded(default, null): Int;

  /**
    The height of the movie clip, in pixels.
  **/
  public var _height: Float;

  /**
  Sets or retrieves the rendering quality used for a SWF file.

  **Deprecated** since Flash Player 7 — This property was deprecated in favor of `MovieClip._quality`.
  **/
  public var _highquality: Float;

  /**
    A Boolean value that specifies what `_root` refers to when a SWF file is loaded into a movie clip.
  **/
  public var _lockroot: Bool;

  /**
    The instance name of the movie clip.
  **/
  public var _name: String;

  /**
    A reference to the movie clip or object that contains the current movie clip or object.
  **/
  public var _parent: MovieClip;

  /**
    Sets or retrieves the rendering quality used for a SWF file.
  **/
  public var _quality: String;

  /**
    Specifies the rotation of the movie clip, in degrees, from its original orientation.
  **/
  public var _rotation: Float;

  /**
    Specifies the number of seconds a sound prebuffers before it starts to stream.
  **/
  public var _soundbuftime: Float;

  /**
    Returns the target path of the movie clip instance, in slash notation.
  **/
  public var _target(default, null): String;

  /**
    The total number of frames in the movie clip instance.
  **/
  public var _totalframes(default, null): Int;

  /**
    Retrieves the URL of the SWF, JPEG, GIF, or PNG file from which the movie clip was downloaded.
  **/
  public var _url(default, null): String;

  /**
    A Boolean value that indicates whether the movie clip is visible.
  **/
  public var _visible: Bool;

  /**
    The width of the movie clip, in pixels.
  **/
  public var _width: Float;

  /**
    An integer that sets the x coordinate of a movie clip relative to the local coordinates of the parent movie clip.
  **/
  public var _x: Float;

  /**
    Returns the x coordinate of the mouse position.
  **/
  public var _xmouse(default, null): Float;

  /**
    Determines the horizontal scale (`percentage`) of the movie clip as applied from the registration point of the movie clip.
  **/
  public var _xscale: Float;

  /**
    Sets the y coordinate of a movie clip relative to the local coordinates of the parent movie clip.
  **/
  public var _y: Float;

  /**
    Indicates the y coordinate of the mouse position.
  **/
  public var _ymouse(default, null): Float;

  /**
    Sets the vertical scale (`percentage`) of the movie clip as applied from the registration point of the movie clip.
  **/
  public var _yscale: Float;

  /**
    The blend mode for this movie clip.
  **/
  public var blendMode: Dynamic;

  /**
    If set to `true`, Flash Player caches an internal bitmap representation of the movie clip.
  **/
  public var cacheAsBitmap: Bool;

  /**
    A Boolean value that indicates whether a movie clip is enabled.
  **/
  public var enabled: Bool;

  /**
    An indexed array containing each filter object currently associated with the movie clip.
  **/
  public var filters: Array<BitmapFilter>;

  /**
    Specifies whether you can programmatically assign focus to a movie clip using Selection.setFocus().
  **/
  public var focusEnabled: Bool;

  /**
    A Boolean value that determines whether images that are added through the `loadMovie()` method and are in the same hierarchy level as the movie clip are smoothed when scaled.
  **/
  public var forceSmoothing: Bool;

  /**
    Designates another movie clip to serve as the hit area for a movie clip.
  **/
  public var hitArea: MovieClip;

  /**
    Associates the specified ContextMenu object with the movie clip.
  **/
  public var menu: ContextMenu;

  /**
    The color of the movie clip's opaque (not transparent) background of the color specified by the number (an RGB hexadecimal value).
  **/
  public var opaqueBackground: Int;

  /**
    The rectangular region that defines the nine scaling regions for the movie clip.
  **/
  public var scale9Grid: Rectangle<Float>;

  /**
    The `scrollRect` property allows you to quickly scroll movie clip content and have a window viewing larger content.
  **/
  public var scrollRect: Dynamic;

  /**
    Determines whether the children of a movie clip are included in the automatic tab ordering.
  **/
  public var tabChildren: Bool;

  /**
    Specifies whether the movie clip is included in automatic tab ordering.
  **/
  public var tabEnabled: Bool;

  /**
    Lets you customize the tab ordering of objects in a movie.
  **/
  public var tabIndex: Int;

  /**
    A Boolean value that indicates whether other buttons or movie clips can receive mouse release events.
  **/
  public var trackAsMenu: Bool;

  /**
    An object with properties pertaining to a movie clip's matrix, color transform, and pixel bounds.
  **/
  public var transform: Transform;

  /**
    A Boolean value that indicates whether the pointing hand (hand cursor) appears when the mouse rolls over a movie clip.
  **/
  public var useHandCursor: Bool;

  /**
    Returns properties that are the minimum and maximum x and y coordinate values of the movie clip, based on the bounds parameter.
  **/
  public function getBounds(bounds: MovieClip): MovieClipBounds;

  /**
    Returns the number of bytes that have already loaded (streamed) for the movie clip.
  **/
  public function getBytesLoaded(): Int;

  /**
    Returns the size, in bytes, of the movie clip.
  **/
  public function getBytesTotal(): Int;

  /**
    Returns the depth of the movie clip instance.
  **/
  public function getDepth(): Int;

  /**
    Determines if a particular depth is already occupied by a movie clip.
  **/
  public function getInstanceAtDepth(depth: Int): MovieClip;

  /**
    Determines a depth value that you can pass to `MovieClip.attachMovie()`, `MovieClip.duplicateMovieClip()`, or `MovieClip.createEmptyMovieClip()` to ensure that Flash renders the movie clip in front of all other objects on the same level and layer in the current movie clip.
  **/
  public function getNextHighestDepth(): Int;

  /**
    Returns properties that are the minimum and maximum x and y coordinate values of the movie clip, based on the `bounds` parameter, excluding any strokes on shapes.
  **/
  public function getRect(bounds: MovieClip): MovieClipBounds;

  /**
    Returns an integer that indicates the Flash Player version for the movie clip was published.
  **/
  public function getSWFVersion(): Int;

  /**
    Returns a TextSnapshot object that contains the text in all the static text fields in the specified movie clip; text in child movie clips is not included.
  **/
  public function getTextSnapshot(): TextSnapshot;

  /**
    Loads a document from the specified URL into the specified window.
  **/
  public function getURL(url: String, ?window: String, ?method: String): Void;

  /**
    Makes the movie clip in the mc parameter a mask that reveals the calling movie clip.
  **/
  public function setMask(mc: Null<MovieClip>): Void;

  /**
    Converts the `pt` object from Stage (global) coordinates to the movie clip's (local) coordinates.
  **/
  public function globalToLocal(pt: Point2): Void;

  /**
    Converts the `pt` object from the movie clip's (local) coordinates to the Stage (global) coordinates.
  **/
  public function localToGlobal(pt: Point2): Void;

  /**
    Evaluates the movie clip to see if it overlaps or intersects with the hit area that the `target` or `x` and `y` coordinate parameters identify.
  **/
  @:overload(function(target: MovieClip): Bool {})
  public function hitTest(x: Float, y: Float, ?shape: Bool): Bool;

  /**
    Starts playing the SWF file at the specified frame.
  **/
  @:overload(function(frame: String): Void {})
  public function gotoAndPlay(frame: Int): Void;

  /**
    Brings the playhead to the specified frame of the movie clip and stops it there.
  **/
  @:overload(function(frame: String): Void {})
  public function gotoAndStop(frame: Int): Void;

  /**
    Moves the playhead in the timeline of the movie clip.
  **/
  public function play(): Void;

  /**
    Stops the movie clip that is currently playing.
  **/
  public function stop(): Void;

  /**
    Sends the playhead to the next frame and stops it.
  **/
  public function nextFrame(): Void;

  /**
    Sends the playhead to the previous frame and stops it.
  **/
  public function prevFrame(): Void;

  /**
    Lets the user drag the specified movie clip.
  **/
  public function startDrag(?lockCenter: Bool, ?left: Float, ?top: Float, ?right: Float, ?bottom: Float): Void;

  /**
    Ends a `MovieClip.startDrag()` method.
  **/
  public function stopDrag(): Void;

  /**
    Specifies the audio source to be played.
  **/
  public function attachAudio(id: Dynamic): Void;

  /**
    Attaches a bitmap image to a movie clip.
  **/
  public function attachBitmap(bmp: BitmapData, depth: Int, ?pixelSnapping: String, ?smoothing: Bool): Void;

  /**
    Takes a symbol from the library and attaches it to the movie clip.
  **/
  public function attachMovie(id: String, name: String, depth: Int, ?initObject: Dynamic): MovieClip;

  /**
    Creates an empty movie clip as a child of an existing movie clip.
  **/
  public function createEmptyMovieClip(name: String, depth: Int): MovieClip;

  /**
    Creates an instance of the specified movie clip while the SWF file is playing.
  **/
  public function duplicateMovieClip(name: String, depth: Int, ?initObject: Dynamic): MovieClip;

  /**
    Creates a new, empty text field as a child of the movie clip on which you call this method.
  **/
  public function createTextField(instanceName: String, depth: Int, x: Float, y: Float, width: Float, height: Float): TextField;

  /**
    Removes a movie clip instance created with `duplicateMovieClip()`, `MovieClip.duplicateMovieClip()`, `MovieClip.createEmptyMovieClip()`, or `MovieClip.attachMovie()`.
  **/
  public function removeMovieClip(): Void;

  /**
    Swaps the stacking, or depth level (z-order), of this movie clip with the movie clip that is specified by the `target` parameter or with the movie clip that currently occupies the depth level that is specified in the `target` parameter.
  **/
  @:overload(function(mc: MovieClip): Void {})
  public function swapDepths(depth: Int): Void;

  /**
    Reads data from an external file and sets the values for variables in the movie clip.
  **/
  public function loadVariables(url: String, ?method: String): Void;

  /**
    Loads a SWF, JPEG, GIF, or PNG file into a movie clip in Flash Player while the original SWF file is playing.
  **/
  public function loadMovie(url: String, ?method: String): Void;

  /**
    Removes the contents of a movie clip instance.
  **/
  public function unloadMovie(): Void;

  /**
    Indicates the beginning of a new drawing path.
  **/
  public function beginFill(rgb: Int, ?alpha: Float): Void;

  /**
    Fills a drawing area with a bitmap image.
  **/
  public function beginBitmapFill(bmp: BitmapData, ?matrix: Matrix, ?repeat: Bool, ?smoothing: Bool): Void;

  /**
    Indicates the beginning of a new drawing path.
  **/
  public function beginGradientFill(fillType: String, colors: Array<Int>, alphas: Array<Dynamic>, ratios: Array<Dynamic>, matrix: Dynamic, ?spreadMethod: String, ?interpolationMethod: String, ?focalPointRatio: Float): Void;

  /**
    Applies a fill to the lines and curves that were since the last call to `beginFill()` or `beginGradientFill()`.
  **/
  public function endFill(): Void;

  /**
    Specifies a line style that Flash uses for subsequent calls to the `lineTo()` and `curveTo()` methods until you call the `lineStyle()` method with different parameters.
  **/
  public function lineStyle(?thickness: Float, ?rgb: Int, ?alpha: Float, ?pixelHinting: Bool, ?noScale: String, ?capsStyle: String, ?jointStyle: String, ?miterLimit: Float): Void;

  /**
    Specifies a line style that Flash uses for subsequent calls to the `lineTo()` and `curveTo()` methods until you call the `lineStyle()` method or the `lineGradientStyle()` method with different parameters.
  **/
  public function lineGradientStyle(fillType: String, colors: Array<Int>, alphas: Array<Dynamic>, ratios: Array<Dynamic>, matrix: Dynamic, ?spreadMethod: String, ?interpolationMethod: String, ?focalPointRatio: Float): Void;

  /**
    Removes all the graphics created during runtime by using the movie clip draw methods, including line styles specified with `MovieClip.lineStyle()`.
  **/
  public function clear(): Void;

  /**
    Moves the current drawing position to (x, y).
  **/
  public function moveTo(x: Float, y: Float): Void;

  /**
    Draws a line using the current line style from the current drawing position to (x, y); the current drawing position is then set to (x, y).
  **/
  public function lineTo(x: Float, y: Float): Void;

  /**
    Draws a curve using the current line style from the current drawing position to (`anchorX`, `anchorY`) using the control point that ((`controlX`, `controlY`) specifies.
  **/
  public function curveTo(controlX: Float, controlY: Float, anchorX: Float, anchorY: Float): Void;

  /**
    Invoked when a movie clip receives data from a `MovieClip.loadVariables()` call.
  **/
  public dynamic function onData(): Void;

  /**
    Invoked when the mouse button is pressed and the pointer rolls outside the object.
  **/
  public dynamic function onDragOut(): Void;

  /**
    Invoked when the pointer is dragged outside and then over the movie clip.
  **/
  public dynamic function onDragOver(): Void;

  /**
    Invoked repeatedly at the frame rate of the SWF file.
  **/
  public dynamic function onEnterFrame(): Void;

  /**
    Invoked when a movie clip has input focus and user presses a key.
  **/
  public dynamic function onKeyDown(): Void;

  /**
    Invoked when a key is released.
  **/
  public dynamic function onKeyUp(): Void;

  /**
    Invoked when a movie clip loses keyboard focus.
  **/
  public dynamic function onKillFocus(newFocus: Dynamic): Void;

  /**
    Invoked when the movie clip is instantiated and appears in the timeline.
  **/
  public dynamic function onLoad(): Void;

  /**
    Invoked when the mouse button is pressed.
  **/
  public dynamic function onMouseDown(): Void;

  /**
    Invoked when the mouse moves.
  **/
  public dynamic function onMouseMove(): Void;

  /**
    Invoked when the mouse button is released.
  **/
  public dynamic function onMouseUp(): Void;

  /**
    Invoked when the user clicks the mouse while the pointer is over a movie clip.
  **/
  public dynamic function onPress(): Void;

  /**
    Invoked when a user releases the mouse button over a movie clip.
  **/
  public dynamic function onRelease(): Void;

  /**
    Invoked after a user presses the mouse button inside the movie clip area and then releases it outside the movie clip area.
  **/
  public dynamic function onReleaseOutside(): Void;

  /**
    Invoked when a user moves the pointer outside a movie clip area.
  **/
  public dynamic function onRollOut(): Void;

  /**
    Invoked when user moves the pointer over a movie clip area.
  **/
  public dynamic function onRollOver(): Void;

  /**
    Invoked when a movie clip receives keyboard focus.
  **/
  public dynamic function onSetFocus(oldFocus: Dynamic): Void;

  /**
    Invoked in the first frame after the movie clip is removed from the Timeline.
  **/
  public dynamic function onUnload(): Void;
}
