package etwin;

import etwin.ds.Nil;

/**
  The `Symbol` abstract type represents a unique String value.

  `Symbol` instances can be used as field names while greatly reducing the
  risks of conflict with existing names.

  You should avoid using `Symbol` directly and prefer the `WeakMap` and
  `WeakSet` classes built on top of it.

  The implementation is very careful to not rely on the initialization order of
  static values: you can safely use this class at any point in time.
**/
abstract Symbol(String) {
  private static var nextId: Int = getNextId();

  /**
    Creates a new unique `Symbol`.

    An optional description can be specified, to be included in the internal `String` value.
  **/
  public function new(?description: String): Void {
    // The prefix acts as a unique key, and contains a rare character to further reduce collision risks.
    // The `getNextId` functions guarantees that the next id is defined and unique.
    // The suffix is an optional user-defined description.
    this = "Sÿmbol#" + getNextId();
    if (description != null) {
      this += "/" + description;
    }
  }

  /**
    Creates a symbol for the given key.
    
    Unlike `new Symbol(key)`, calling this method multiple times with the same key will
    always return the same symbol.
  **/
  public static inline function for_(key: String): Symbol {
    return cast "Sÿmbol/" + key;
  }

  /**
    Retrieves the key associated with `sym`, if it was created with `Symbol.for_`.
  **/
  public static function keyFor(sym: Symbol): Null<String> {
    var raw: String = cast sym;
    if (raw.indexOf("Sÿmbol/") == 0) {
      return raw.substr(7);
    } else {
      return null;
    }
  }

  /**
    Retrieves symbol from a given `object`.
  **/
  public inline function ofObject<T>(object: {}): Null<T> {
    return Reflect.field(object, this);
  }

  /**
    Returns the internal `String` value of this `Symbol`.
  **/
  public inline function toString(): String {
    return this;
  }

  private static inline function getNextId(): Int {
    // Workaround for static initialization order issues
    if (nextId == null) {
      nextId = 0;
    }
    return nextId++;
  }
}
