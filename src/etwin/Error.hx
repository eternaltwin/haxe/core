package etwin;

#if (flash8 || js)
import etwin.native.Error in NativeError;

typedef Error = NativeError;
#else
class Error {
  public var message: String;
  public var name: String;

  public function new(?message: String): Void {
    this.name = "Error";
    this.message = message != null ? message : "";
  }

  public function toString(): String {
    return this.name + (this.message != "" ? (": " + this.message) : "");
  }
}
#end
