package etwin;

/**
 * Native root object class
 */
@:native("Object")
extern class Object implements Dynamic {
  @:overload(function(a0: Dynamic): Void {})
  public function new(): Void;
}
