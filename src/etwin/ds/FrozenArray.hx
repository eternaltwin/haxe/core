package etwin.ds;

import haxe.macro.Expr;

/**
  An immutable array, whose contents can never change.

  All non-mutating methods of `Array` are available.
**/
@:forward(
  indexOf, iterator, join, lastIndexOf, length, toString,
  // These methods return a writable `Array` (but don't modify _this_ array).
  concat, copy, filter, map, slice
)
abstract FrozenArray<T>(Array<T>) to ReadOnlyArray<T> {
  private static var EMPTY(default, never): FrozenArray<Dynamic> = new FrozenArray([]);

  /**
    Creates a new `FrozenArray` containing the elements of the given iterable or iterator.
  **/
  macro public static function from<T>(items: ExprOf<Iterable<T>>): ExprOf<FrozenArray<T>> {
    return macro new etwin.ds.FrozenArray([for (item in $items) item]);
  }

  /**
    Unsafely converts the given `array` into a `FrozenArray`.

    No copy will be done; you must ensure that `array` won't be further modified.
  **/
  public static inline function uncheckedFrom<T>(array: Array<T>): FrozenArray<T> {
    return new FrozenArray(array);
  }

  /**
    Creates a new `FrozenArray` containing the given elements.
    
    ```hx
    var a: FrozenArray<Int> = FrozenArray.of(1, 5, 10);
    ```
  **/
  macro public static function of<T>(items: Array<ExprOf<T>>): ExprOf<FrozenArray<T>> {
    if (items.length == 0) {
      return macro etwin.ds.FrozenArray.empty();
    }

    return macro new etwin.ds.FrozenArray([$a{items}]);
  }

  /**
    Creates an empty `FrozenArray`.
  **/
  public static inline function empty<T>(): FrozenArray<T> {
    return cast FrozenArray.EMPTY;
  }

  /**
    Variant of `Array.concat` returning a `FrozenArray`.
  **/
  public static inline function concat<T>(a: ReadOnlyArray<T>, b: ReadOnlyArray<T>): FrozenArray<T> {
    return new FrozenArray(a.concat(cast b));
  }

  /**
    Variant of `Array.map` returning a `FrozenArray`.
  **/
  public static inline function map<T, S>(a: ReadOnlyArray<T>, f: T -> S): FrozenArray<S> {
    return new FrozenArray(a.map(f));
  }

  /**
    Variant of `Array.filter` returning a `FrozenArray`.
  **/
  public static inline function filter<T>(a: ReadOnlyArray<T>, f: T -> Bool): FrozenArray<T> {
    return new FrozenArray(a.filter(f));
  }

  /**
    Variant of `Array.slice` returning a `FrozenArray`.
  **/
  public static inline function slice<T>(a: ReadOnlyArray<T>, pos: Int, ?end: Int): FrozenArray<T> {
    return new FrozenArray(a.slice(pos, end));
  }

  /**
    Variant of `Array.reverse` returning a `FrozenArray`.
  **/
  public static inline function reverse<T>(a: ReadOnlyArray<T>): FrozenArray<T> {
    var copy = a.copy();
    copy.reverse();
    return new FrozenArray(copy);
  }

  /**
    Variant of `Array.sort` returning a `FrozenArray`.
  **/
  public static inline function sort<T>(a: ReadOnlyArray<T>, f: T -> T -> Int): FrozenArray<T> {
    var copy = a.copy();
    copy.sort(f);
    return new FrozenArray(copy);
  }

  private inline function new(array: Array<T>): Void {
    this = array;
  }

  @:arrayAccess
  private inline function get(i: Int): T {
    return this[i];
  }
}
