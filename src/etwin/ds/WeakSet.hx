package etwin.ds;

import etwin.Symbol;

/**
  The WeakSet abstract type allows to flag arbitrary objects.
 
  A WeakSet can be used to safely attach a flag to an object.
**/
abstract WeakSet<T: {}>(Symbol) {
  public inline function new(?name: String): Void {
    this = new Symbol(name);
  }

  public inline function add(o: T): Void {
    Reflect.setField(o, this.toString(), true);
    #if flash8
    // Make the property non-enumerable
    untyped ASSetPropFlags(o, this.toString(), 1);
    #end
  }

  public inline function exists(o: T): Bool {
    return Reflect.field(o, this.toString()) != null;
  }

  public inline function remove(o: T): Void {
    Reflect.deleteField(o, this.toString());
  }
}
