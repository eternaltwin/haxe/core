package etwin.ds;

/**
  A read-only array, which cannot be directly mutated.

  Unlike `FrozenArray`, contents may still be indirectly
  mutated through other references to the underlying array.

  All non-mutating methods of `Array` are available.
**/
@:forward(
  indexOf, iterator, join, lastIndexOf, length, toString,
  // These methods return a writable `Array` (but don't modify _this_ array).
  concat, copy, filter, map, slice
)
abstract ReadOnlyArray<T>(Array<T>) from Array<T> {
  @:arrayAccess @:dox(hide)
  private inline function get(i: Int): T {
    return this[i];
  }

  @:to @:noCompletion @:dox(hide)
  public inline function toIterable(): Iterable<T> {
    return cast this;
  }
}
