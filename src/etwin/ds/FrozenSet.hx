package etwin.ds;

import haxe.macro.Expr.ExprOf;

/**
  An immutable set, whose contents can never change.

  All non-mutating methods of `Set` are available.
**/
@:forward(exists, iterator, toString)
abstract FrozenSet<T>(Set<T>) to ReadOnlySet<T> {
  // Using String instead of Dynamic because Set<Dynamic> doesn't exist
  private static var EMPTY(default, never): FrozenSet<String> = new FrozenSet(new Set());

  private inline function new(set: Set<T>) {
    this = set;
  }

  /**
    Creates a new `FrozenSet` containing the elements of the given iterable or iterator.
  **/
  macro public static function from<T>(items: ExprOf<Iterable<T>>): ExprOf<FrozenSet<T>> {
    return macro etwin.ds.FrozenSet.uncheckedFrom(etwin.ds.Set.from($items));
  }

  /**
    Converts the given `set` into a `FrozenSet`.

    No copy will be done; you must ensure that `set` won't be further modified.
  **/
  public static inline function uncheckedFrom<T>(set: Set<T>): FrozenSet<T> {
    return new FrozenSet(set);
  }

  /**
    Creates a new `FrozenSet` containing the given elements.

    ```hx
    var set: FrozenSet<String> = FrozenSet.of("foo", "bar");
    ```
  **/
  macro public static function of<T>(items: Array<ExprOf<T>>): ExprOf<FrozenSet<T>> {
    if (items.length == 0) {
      return macro etwin.ds.FrozenSet.empty();
    }

    return macro etwin.ds.FrozenSet.uncheckedFrom(etwin.ds.Set.of($a{items}));
  }

  /**
    Creates an empty `FrozenSet`.
  **/
  public static inline function empty<T>(): FrozenSet<T> {
    return cast FrozenSet.EMPTY;
  }
}
